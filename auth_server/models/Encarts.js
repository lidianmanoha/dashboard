const mongoose = require('mongoose');
const slugify = require('slugify');

const EncartsSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Please add a name'],
    unique: true,
    maxlength: [50, 'Name'],
  },
  slug: String,
  inProgress: {
    type: Boolean,
  },
  subtitle: {
    type: String,
    required: [true, 'Please add a subtitle'],
    maxlength: [100, 'subtitle can not be more than 100 characters'],
  },
  text: {
    type: String,
    required: [true, 'Please add a text'],
    maxlength: [500, 'text can not be more than 500 characters'],
  },

  btn: {
    type: String,
    required: [true, 'Please add btn name'],
  },
  url: {
    type: String,
  },
  image: {
    type: String,
    default: 'no-image.png',
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

// Create Encarts slug from the name
EncartsSchema.pre('save', function (next) {
  this.slug = slugify(this.title, { lower: true });
  next();
});

module.exports = mongoose.model('Encarts', EncartsSchema);
