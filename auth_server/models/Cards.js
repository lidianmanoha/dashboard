const mongoose = require('mongoose');
const slugify = require('slugify');

const CardsSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Please add a name project'],
    unique: true,
    maxlength: [50, 'Name project'],
  },
  slug: String,
  inProgress: {
    type: Boolean,
  },
  description: {
    type: String,
    required: [true, 'Please add a description'],
    maxlength: [500, 'Description can not be more than 500 characters'],
  },
  techno: [
    {
      techLogo: {
        type: String,
        require: [true, 'Please add the technologies used'],
      },
      techName: {
        type: String,
        require: [true, 'Please add the technologies used'],
      },
      techLink: { type: String },
    },
  ],

  linkWebSite: {
    type: String,
    required: [true, 'Please add website project'],
  },
  image: {
    type: String,
    default: 'no-image.png',
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

// Create Cards slug from the name
CardsSchema.pre('save', function (next) {
  this.slug = slugify(this.title, { lower: true });
  next();
});

module.exports = mongoose.model('Cards', CardsSchema);
