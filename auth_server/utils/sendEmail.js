const nodemailer = require("nodemailer");
const hbs = require("nodemailer-express-handlebars");
const path = require("path");
const sendEmail = async (options) => {
	let transporter = nodemailer.createTransport({
		host: process.env.SMTP_HOST,
		port: process.env.SMTP_PORT,
		auth: {
			user: process.env.SMTP_EMAIL,
			pass: process.env.SMTP_PASSWORD,
		},
		tls: {
			rejectUnauthorized: false,
		},
	});

	transporter.use(
		"compile",
		hbs({
			viewEngine: {
				extName: ".hbs",
				partialsDir: path.join(__dirname, "../views"),
				layoutsDir: path.join(__dirname, "../views"),
				defaultLayout: null,
			},
			viewPath: path.join(__dirname, "../views/"),
			extName: ".hbs",
		}),
	);

	const message = {
		from: `${process.env.FROM_NAME} <${process.env.FROM_EMAIL}>`,
		to: options.email,
		subject: options.subject,
		text: options.message,
		template: options.template,
		context: options.context,
		attachments: [
			{
				filename: "fileName.pdf",
				contentType: "application/pdf",
			},
			{
				filename: "no-image.png",
				path: process.env.FILE_UPLOAD_PATH + "/no-image.png",
			},
		],
	};

	// send mail with defined transport object
	transporter.sendMail(message, (error, info) => {
		if (error) {
			return console.log(error);
		}
		console.log("Message sent: %s", info.messageId);
		console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
	});
};

module.exports = sendEmail;
