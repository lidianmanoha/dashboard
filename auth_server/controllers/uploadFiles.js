const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const _ = require("lodash");
// @Desc        Get file
// @Route       GET /api/v1/uploadFiles
// @access      Public
exports.getUploadFile = (req, res, next) => {
	res.status(200).json({ success: true, msg: "Show uploadFiles" });
};

// @Desc        Post file
// @Route       POST /api/v1/uploadFiles/file
// @access      Public
exports.postFile = asyncHandler(async (req, res, next) => {
	if (!req.files) {
		res.send({
			status: false,
			message: "No file uploaded",
		});
	} else {
		//Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
		let file = req.files.file;

		// Make sure the image is a photo
		if (!file.mimetype.startsWith("image")) {
			return next(new ErrorResponse(`Please upload an image file`, 400));
		}

		// Check filesize
		if (file.size > process.env.MAX_FILE_UPLOAD) {
			return next(new ErrorResponse(`Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`, 400));
		}
		//Use the mv() method to place the file in upload directory (i.e. "uploads")
		// file.mv(process.env.FILE_UPLOAD_PATH + file.name);
		file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async (err) => {
			if (err) {
				console.error(err);
				return next(new ErrorResponse(`Problem with file upload`, 500));
			}
		});
		//send response
		res.send({
			status: true,
			message: "File is uploaded",
			data: {
				name: file.name,
				mimetype: file.mimetype,
				size: file.size,
			},
		});
	}
});

// @Desc        Post files
// @Route       POST /api/v1/uploadFiles/files
// @access      Public
exports.postFiles = asyncHandler(async (req, res, next) => {
	if (!req.files) {
		res.send({
			status: false,
			message: "No file uploaded",
		});
	} else {
		let data = [];

		//loop all files
		_.forEach(_.keysIn(req.files.files), (key) => {
			let file = req.files.files[key];

			// Make sure the image is a photo
			if (!file.name.match(/\.(png|PNG|jpeg|JPEG|jpg|JPG|pdf)$/)) {
				return next(new ErrorResponse(`Please upload an image file`, 400));
			}

			// Check filesize
			if (file.size > process.env.MAX_FILE_UPLOAD) {
				return next(new ErrorResponse(`Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`, 400));
			}

			//move photo to uploads directory
			file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async (err) => {
				if (err) {
					console.error(err);
					return next(new ErrorResponse(`Problem with file upload`, 500));
				}
			});
			//push file details
			data.push({
				name: file.name,
				mimetype: file.mimetype,
				size: file.size,
			});
		});

		//send response
		res.send({
			status: true,
			message: "File is uploaded",
			data: data,
		});
	}
});
