const sendEmail = require("../utils/sendEmail");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");

// @Desc        Get dashboard
// @Route       GET /api/v1/dashboard
// @access      Public
exports.getDashboard = (req, res, next) => {
	res.status(200).json({ success: true, msg: "Show auth dashboard" });
};

// @Desc        post send
// @Route       GET /api/v1/dashboard/send
// @access      Public
exports.postSend = asyncHandler(async (req, res, next) => {
	const output = {
		gender: req.body.gender,
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		phone: req.body.phone,
		message: req.body.message,
	};
	try {
		await sendEmail({
			email: "contact@irimax-dev.fr",
			subject: `Message de ${req.body.gender} ${req.body.firstName} ${req.body.lastName}`,
			template: "contact/index",
			context: output,
		});
		console.log(output);
		return res.status(200).json({ success: true, data: "Email sent" });
	} catch (err) {
		console.log(err);

		return next(new ErrorResponse("Email could not be sent", 500));
	}
});
