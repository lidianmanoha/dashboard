const path = require('path');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Encarts = require('../models/Encarts');

// @Desc        Get all Encarts
// @Route       GET /api/v1/Encarts
// @access      Public
exports.getEncarts = asyncHandler(async (req, res, next) => {
  const encarts = await Encarts.find();
  res.status(200).json({ success: true, data: encarts });
});

// @Desc        Get all Encart
// @Route       GET /api/v1/Encart/all
// @access      Private
exports.getAllEncarts = asyncHandler(async (req, res, next) => {
  const encarts = await Encarts.find();
  res.status(200).json({ success: true, data: encarts });
});

// @Desc        Get single Encart by id
// @Route       GET /api/v1/Encart/:id
// @access      Private
exports.getEncart = asyncHandler(async (req, res, next) => {
  const encarts = await Encarts.findById(req.params.id);
  if (!encarts) {
    return next(
      next(
        new ErrorResponse(`Encart not found with id of ${req.params.id}`, 404),
      ),
    );
  }
  res.status(200).json({ success: true, data: encarts });
});

// @Desc        Create Encart
// @Route       POST /api/v1/Encart
// @access      Private
exports.createEncart = asyncHandler(async (req, res, next) => {
  //  Add user to req.body
  req.body.user = req.user.id;

  //  Check for published Encart
  const publishedEncart = await Encarts.findOne({ user: req.user.id });

  //  If the user is not an admin, theycan only add one Encart
  if (publishedEncart && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(
        `The user with ID ${req.user.id} has already published a Encart`,
        400,
      ),
    );
  }

  const encarts = await Encarts.create(req.body);
  res.status(201).json({
    success: true,
    data: encarts,
  });
});

// @Desc        Update Encart by id
// @Route       PUT /api/v1/Encart/:id
// @access      Private
exports.updateEncart = asyncHandler(async (req, res, next) => {
  let encarts = await Encarts.findById(req.params.id);

  if (!encarts) {
    return next(
      new ErrorResponse(`Encart not found with id of ${req.params.id}`, 404),
    );
  }

  //  Make sure user is Encart owner
  if (encarts.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(
        `User ${req.params.id} is not authorized to update this Encart`,
        401,
      ),
    );
  }

  encarts = await Encarts.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({ success: true, data: encarts });
});

// @Desc        Delete Encart by id
// @Route       DELETE /api/v1/Encart/:id
// @access      Private
exports.deleteEncart = asyncHandler(async (req, res, next) => {
  const encarts = await Encarts.findById(req.params.id);
  if (!encarts) {
    return next(
      next(
        new ErrorResponse(`Encart not found with id of ${req.params.id}`, 404),
      ),
    );
  }
  //  Make sure user is Encart owner
  if (encarts.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(
        `User ${req.params.id} is not authorized to delete this Encart`,
        401,
      ),
    );
  }
  encarts.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

// @desc      Upload photo for Encart
// @route     PUT /api/v1/Encart/:id/photo
// @access    Private
exports.EncartPhotoUpload = asyncHandler(async (req, res, next) => {
  const encarts = await Encarts.findById(req.params.id);

  if (!encarts) {
    return next(
      new ErrorResponse(`Encart not found with id of ${req.params.id}`, 404),
    );
  }

  //  Make sure user is Encart owner
  if (encarts.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(
        `User ${req.params.id} is not authorized to delete this Encart`,
        401,
      ),
    );
  }

  if (!req.files) {
    return next(new ErrorResponse(`Please upload a file`, 400));
  }

  const file = req.files.file;

  // Make sure the image is a photo
  if (!file.mimetype.startsWith('image')) {
    return next(new ErrorResponse(`Please upload an image file`, 400));
  }

  // Check filesize
  if (file.size > process.env.MAX_FILE_UPLOAD) {
    return next(
      new ErrorResponse(
        `Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`,
        400,
      ),
    );
  }

  // Create custom filename
  file.name = `photo_${encarts._id}${path.parse(file.name).ext}`;

  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async (err) => {
    if (err) {
      console.error(err);
      return next(new ErrorResponse(`Problem with file upload`, 500));
    }

    await Encarts.findByIdAndUpdate(req.params.id, { image: file.name });

    res.status(200).json({
      success: true,
      data: file.name,
    });
  });
});
