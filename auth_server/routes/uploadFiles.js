const express = require("express");
const { getUploadFile, postFile, postFiles } = require("../controllers/uploadFiles");
const router = express.Router();

router.get("/", getUploadFile).post("/file", postFile).post("/files", postFiles);

module.exports = router;
