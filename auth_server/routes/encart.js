const express = require('express');
const {
  getEncart,
  getEncarts,
  getAllEncarts,
  createEncart,
  updateEncart,
  deleteEncart,
  EncartPhotoUpload,
} = require('../controllers/Encart');

const router = express.Router();

const { protect, authorize } = require('../middleware/auth');

router
  .route('/')
  .get(getEncarts)
  .post(protect, authorize('publisher', 'admin'), createEncart);
router.route('/all').get(getAllEncarts);
router
  .route('/:id/photo')
  .put(protect, authorize('publisher', 'admin'), EncartPhotoUpload);

router
  .route('/:id')
  .get(getEncart)
  .put(protect, authorize('publisher', 'admin'), updateEncart)
  .delete(protect, authorize('publisher', 'admin'), deleteEncart);

module.exports = router;
