const express = require("express");
const { getDashboard, postSend } = require("../controllers/dashboard");
const router = express.Router();

router.get("/", getDashboard).post("/send", postSend);

module.exports = router;
