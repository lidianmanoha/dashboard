import React from 'react';
import ClassNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../Button/button.styles.scss';
import { HashLink as Link } from 'react-router-hash-link';
const ButtonComponent = (props) => {
  const { text_btn, linkUrlclick, icon, btn } = props;

  return (
    <Link to={linkUrlclick}>
      <button  className={ClassNames(btn === true ? 'btn' : 'btn2')}>
        {text_btn}
        {!icon && (
          <FontAwesomeIcon
            icon={['fad', 'angle-double-right']}
            color='rgb(24, 163, 255);'
            size='lg'
            className='info'
          />
        )}
      </button>
    </Link>
  );
};

export default ButtonComponent;
