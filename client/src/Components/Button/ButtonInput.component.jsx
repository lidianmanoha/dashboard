import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../Button/button.styles.scss';

const ButtonComponent = ({ text_btn, typeBtn }) => {
  return (
    <button className='btn' type={typeBtn}>
      {text_btn}
      <FontAwesomeIcon
        icon={['fad', 'angle-double-right']}
        color='rgb(24, 163, 255);'
        size='lg'
        className='info'
      />
    </button>
  );
};

export default ButtonComponent;
