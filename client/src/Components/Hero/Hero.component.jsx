import React, { useEffect } from 'react';
import Video from '../../assets/video/Blue.mp4';

import './hero.styles.scss';
const Hero = () => {
  const [width, setWidth] = React.useState(window.innerWidth);
  const breakpoint = 800;

  useEffect(() => {
    const handleWindowResize = () => setWidth(window.innerWidth);
    window.addEventListener('resize', handleWindowResize);

    // Return a function from the effect that removes the event listener
    return () => window.removeEventListener('resize', handleWindowResize);
  }, []);
  return (
    <div className='container_hero'>
      {width < breakpoint ? (
        <video
          width='100%'
          height='100%'
          loop
          muted
        >
          <source src={Video} type='video/mp4' />
        </video>
      ) : (
        <video
          width='100%'
          height='100%'
          loop
          autoPlay
          muted
        >
          <source src={Video} type='video/mp4' />
        </video>
      )}
      <div className='container_text'>
        <h1 className='text_hero'>Agence Digitale et de conseil</h1>
        <h2 className='text_hero_2'>
          Vous recherchez a améliorer votre visibilité sur la toile ? où vous
          avez une idée d’application que vous souhaitez concrétisez, nous
          sommes là pour vous conseiller et vous aidé a réalisez vos projets.
        </h2>
      </div>
    </div>
  );
};

export default Hero;
