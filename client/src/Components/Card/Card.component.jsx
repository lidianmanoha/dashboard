import React, { useState, useEffect } from "react";
import ClassNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import ReadMoreAndLess from "react-read-more-less";
import axios from "axios";
// import cardsLists from '../../_data/card.json';

// Loading
import Spinner from "../Spinner/Spinner.component";

// Aos Animation
import Aos from "aos";
import "aos/dist/aos.css";

// Css Styles
import "./card.styles.scss";

const Card = ({ techno, webLink, cardsLists, imageBaseURL }) => {
	useEffect((err) => {
		Aos.init({ once: true });
	}, []);

	return (
		<div className='container-card'>
			{cardsLists &&
				cardsLists.map((cardsList) => (
					<div key={cardsList._id} className='wrapper' data-aos='zoom-in-up' data-aos-easing='ease-out-cubic' data-aos-duration='3000'>
						<div className='card'>
							<div>
								<img className='card_image' src={`${imageBaseURL}${cardsList.image}`} alt='zen' />
							</div>
							<div className='card_name'>{cardsList.title}</div>
							<div className='card_type'>{cardsList.inProgress === true ? "En cours de réalisation" : ""}</div>
							<div className='card_description'>
								{/* <ReadMoreAndLess
									className='read-more-content'
									charLimit={150}
									readMoreText={<FontAwesomeIcon icon={["far", "caret-circle-down"]} size='lg' />}
									readLessText={<FontAwesomeIcon icon={["far", "caret-circle-up"]} size='lg' />}
								>
									{cardsList.description}
								</ReadMoreAndLess> */}
							</div>
							{!webLink ? (
								<div className='link_card'>
									<a target='blank' href={cardsList.linkWebSite}>
										{/* <FontAwesomeIcon
                  icon={['fad', 'link']}
                  size='xs'
                  className='icon_link'
                /> */}
										Voir le site
									</a>
								</div>
							) : (
								""
							)}

							{!techno ? (
								<div className={ClassNames("card_tech", "giant", "clearfix")}>
									{cardsList.techno.map((tech) => (
										<div key={tech._id} className={ClassNames("one-third", "no-border")}>
											<div className='stat'>
												<a href={tech.techLink}>
													<FontAwesomeIcon
														icon={[`${tech.techLogo}` === "store" ? "fad" : `${tech.techLogo}` === "database" ? "fad" : "fab", `${tech.techLogo}`]}
														size='lg'
														className={tech.techName}
													/>
												</a>
											</div>

											<div className='stat-value'>{tech.techName === "store" ? "Woo" : tech.techName === "database" ? "MongoDB" : tech.techName}</div>
										</div>
									))}
								</div>
							) : (
								""
							)}
						</div>
					</div>
				))}
		</div>
	);
};

export default Card;
