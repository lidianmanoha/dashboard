import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import './menuBurger.styles.scss';

class MenuBurger extends Component {
  constructor(props) {
    super(props);

    this.setMenu = this.setMenu.bind(this);
  }
  //
  // Close Menu Click
  //

  componentDidMount() {
    const menu = document.querySelector('.menu');
    menu.addEventListener('click', this.setMenu);
  }

  setMenu() {
    const inputs = document.getElementsByTagName('input');

    for (let i = 0; i < inputs.length; i++) {
      if (inputs[i].type === 'checkbox') {
        if ((inputs[i].checked = false)) {
          inputs[i].checked = true;
        } else {
          if ((inputs[i].checked = true)) {
            inputs[i].checked = false;
          }
        }
      }
    }
  }

  render() {
    return (
      <div className='menu-wrap'>
        <input type='checkbox' className='toggler'></input>
        <div className='hamburger'>
          <div></div>
        </div>
        <div className='menu'>
          <div>
            <div>
              <ul id='set-menu'>
                <li className='nav-item'>
                  <NavLink
                    className='menu-link'
                    to='/'
                    exact
                    activeClassName='active'
                  >
                    Home
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='menu-link'
                    to='/Prestation'
                    exact
                    activeClassName='active'
                  >
                    Préstation
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='menu-link'
                    to='/Portfolio'
                    exact
                    activeClassName='active'
                  >
                    Références
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='menu-link'
                    to='/Contact'
                    exact
                    activeClassName='active'
                  >
                    Contact
                  </NavLink>
                </li>
                <li className='nav-item'>
                  <NavLink
                    className='menu-link'
                    to='/aboutUs'
                    exact
                    activeClassName='active'
                  >
                    About us
                  </NavLink>
                </li>
              </ul>
              {/* <ul id='social-media'>
                <li className='nav-item'>
                  <a href='https://github.com/Irimax' target='blank'>
                    <i className='fab fa-github fa-2x'></i>
                  </a>
                  <a
                    href='http://www.linkedin.com/in/lidian-manoha-2839b8111/n'
                    target='blank'
                  >
                    <i className='fab fa-linkedin fa-2x'></i>
                  </a>
                </li>
              </ul> */}
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MenuBurger.protoTypes = {
  Name: PropTypes.string,
  isChecked: PropTypes.bool,
};
export default MenuBurger;
