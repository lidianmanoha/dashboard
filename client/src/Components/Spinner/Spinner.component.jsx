import React from 'react';

// Css
import './spinner.styles.scss';

const Spinner = (props) => {
  const { wave } = props;
  return (
    <div className='container_spinner'>
      <div className={wave === true ? 'loadingWave' : 'loading'}>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  );
};

export default Spinner;
