import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';

import './menu.styles.scss';

const Header = () => {
  return (
    <Fragment>
      <div className='container_menu'>
        <div className='menu-item'>
          <NavLink className='menu-link' to='/' exact activeClassName='active'>
            Home
          </NavLink>
          <NavLink
            className='menu-link'
            to='/PricePrestation'
            exact
            activeClassName='active'
          >
            Tarif & Prestation
          </NavLink>
          <NavLink
            className='menu-link'
            to='/Portfolio'
            exact
            activeClassName='active'
          >
            Références
          </NavLink>
          <NavLink
            className='menu-link'
            to='/Contact'
            exact
            activeClassName='active'
          >
            Contact
          </NavLink>
          <NavLink
            className='menu-link'
            to='/aboutUs'
            exact
            activeClassName='active'
          >
            About us
          </NavLink>
        </div>
      </div>
      {/* <MenuBurger/> */}
    </Fragment>
  );
};

export default Header;
