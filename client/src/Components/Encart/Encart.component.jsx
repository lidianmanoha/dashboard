import React, { useState, useEffect } from "react";
import Btn from "../Button/Button.component";
import axios from "axios";

// Aos Animation
import Aos from "aos";
import "aos/dist/aos.css";

// Css Styles
import "./encart.styles.scss";

// import dataEncarts from '../../_data/slide.json';

const Encart = () => {
	const [dataEncarts, setDataencarts] = useState([]);

	useEffect((err) => {
		axios
			.get("https://irimaxserver.irimax-dev.fr/api/v1/encarts")
			.then((res) => {
				// console.log(res);
				setDataencarts(res.data.data);
			})
			.catch(err);
	}, []);
	// useEffect(() => {
	//   Aos.init({ once: true, disable: true | "mobile" | "phone" | "tablet" | (() => true)});
	// }, []);
	return (
		<div>
			{dataEncarts.map((dataEncart) => (
				<div key={dataEncart._id} className='content_encart'>
					<div
						className='content_info'
						// data-aos='zoom-in'
						// data-aos-duration='2000'
					>
						<h1 className='text-info'>{dataEncart.title}</h1>
						<h3 className='text-subtitle'>{dataEncart.subtitle}</h3>
						<p className='text'>{dataEncart.text}</p>

						<div className='btn_text'>{dataEncart.btn ? <Btn btn text_btn={dataEncart.btn} linkUrlclick={dataEncart.url} /> : ""}</div>
					</div>
					<div className='img_encart_content'>
						<img
							// data-aos='flip-left'
							// data-aos-easing="ease-in-sine"
							// data-aos-duration='2000'
							src={dataEncart.image}
							alt={dataEncart.title}
							className='img-encart'
						/>
					</div>
				</div>
			))}
		</div>
	);
};
export default Encart;
