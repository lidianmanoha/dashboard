import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import './googleMap.styles.scss';
import Marker from './Marker'

class SimpleMap extends Component {
  static defaultProps = {
    center: {
      lat: 50.386595,
      lng: 3.602268,
    },
    zoom: 11,
  };

  render() {
    return (
      // Important! Always set the container height explicitly
      <div className='container_map'>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyDVT-wQ9tUKMgldi4Rbcf3NgnM9fRawSNU'  }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          yesIWantToUseGoogleMapApiInternals
        >
          <Marker
            lat={50.386595}
            lng={3.602268}
            text="i'm here"
            color="rgb(24, 163, 255)"
          />
        </GoogleMapReact>
      </div>
    );
  }
}

export default SimpleMap;
