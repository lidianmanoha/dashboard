import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import Media from 'react-media';
import MenuBurger from '../MenuBurger/MenuBurger.component';
import Menu from '../Menu/Menu.component';
import { ReactComponent as Logo } from '../../assets/img/logo.svg';

import './header.styles.scss';

const Header = () => {
  return (
    <Fragment>
      <div></div>
      <div className='container_header'>
        <div className='logo-container'>
          <NavLink to='/'>
            <Logo className='logo' />
          </NavLink>
        </div>

        <Media
          queries={{
            phone: '(max-width: 768px)',
            medium: '(min-width: 769px) and (max-width: 1199px)',
            large: '(min-width: 1200px)',
          }}
        >
          {(matches) => (
            <Fragment>
              {matches.phone && <MenuBurger />}
              {matches.medium && <Menu />}
              {matches.large && <Menu />}
            </Fragment>
          )}
        </Media>
      </div>
    </Fragment>
  );
};

export default Header;
