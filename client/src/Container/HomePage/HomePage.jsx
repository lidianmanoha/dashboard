import React from 'react';
import Encart from '../../Components/Encart/Encart.component';

import Hero from '../../Components/Hero/Hero.component';

import './homepage.styles.scss';

const HomePage = () => {
  return (
    <div>
      <Hero />
      <Encart />
    </div>
  );
};

export default HomePage;
