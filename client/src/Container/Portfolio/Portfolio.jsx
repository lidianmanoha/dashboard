import React, { useState, useEffect } from 'react';
import Card from '../../Components/Card/Card.component';
import axios from 'axios';

// Loading
import Spinner from '../../Components/Spinner/Spinner.component';

// Css
import './portfolio.styles.scss';

const Portfolio = () => {
  // Directory Images
  const imageBaseURL = 'https://irimaxserver.irimax-dev.fr/uploads/';

  const [cardsLists, setCardsLists] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect((err) => {
    setIsLoading(true);
    axios
      .get('https://irimaxserver.irimax-dev.fr/api/v1/cards')

      .then((res) => {
        console.log('*******************IRIMAX*******************');
        console.log(res);
        console.log('*******************IRIMAX*******************');
        setIsLoading(false);
        setCardsLists(res.data.data);
      })
      .catch(err);
  }, []);

  if (isLoading) {
    return (
      <div>
        <h1 className='title_portfolio'>Ils nous ont fait confiance !</h1>
        <Spinner />;
      </div>
    );
  } else {
    return (
      <div>
        <h1 className='title_portfolio'>Ils nous ont fait confiance !</h1>

        <Card cardsLists={cardsLists} imageBaseURL={imageBaseURL} />
      </div>
    );
  }
};

export default Portfolio;
