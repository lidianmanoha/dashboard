import React from 'react';

import './aboutUs.styles.scss';
const AboutUs = () => {
  return (
    <div className='container_aboutUS'>
      <p className='lorem'>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vel
        quasi veniam incidunt asperiores molestiae adipisci esse placeat aliquid
        assumenda beatae amet qui, perferendis animi distinctio eveniet maiores
        rerum doloremque officia natus ut laborum? Eaque sunt cum, commodi totam
        consectetur neque quaerat excepturi mollitia dolores, nisi autem
        accusantium id, ipsa ea rerum. Atque odio expedita rerum incidunt
        eveniet saepe, maiores ipsam eum officiis rem blanditiis, laudantium
        dolorem eius quos nemo in totam nesciunt praesentium voluptate. Facilis
        natus nesciunt fugit impedit deleniti nostrum minima iste illo iusto?
        Adipisci necessitatibus quia nesciunt odio nulla repellendus quasi nemo
        maxime sapiente eveniet? Numquam, eos necessitatibus tempore, iure
        tenetur ipsam, ipsum recusandae corrupti dolor ex cupiditate. Ipsum
        impedit iusto sequi amet ducimus itaque explicabo? Ratione atque eos nam
        possimus blanditiis similique autem placeat aut, quae aliquid. Atque
        perspiciatis adipisci corporis officiis quo ratione minima ex, porro
        iusto eos officia aut sint nemo doloribus repellendus doloremque nisi
        nulla temporibus. Non dolorem, quam perferendis explicabo praesentium
        nobis adipisci architecto consequatur illum eum incidunt, deleniti
        tenetur quibusdam velit molestias fuga. Eligendi ad aperiam iste earum
        dicta, tempora beatae mollitia, eos quasi id odio, sit iusto pariatur
        unde veritatis neque hic ducimus facilis adipisci natus. Sapiente
        molestias incidunt nesciunt est voluptas eaque modi qui pariatur sequi,
        ex optio perferendis illum vel corporis doloremque quae vitae tempore
        vero aspernatur error! In sed saepe voluptates, asperiores, obcaecati
        laboriosam dignissimos tempore nostrum pariatur ipsum dicta
        exercitationem animi. Quod amet nulla est. Corporis nobis suscipit illo
        nisi eum natus nesciunt dolores minima doloremque repellendus porro,
        eius culpa, quam similique provident facilis rerum sunt quaerat
        officiis, cum sint id amet temporibus? Quasi non accusantium rem
        possimus sunt error blanditiis eveniet expedita accusamus corporis, quas
        fuga ipsam perferendis aliquid et a sapiente neque odio harum,
        veritatis, repellat quod? Porro illo libero molestiae repellendus
        consectetur atque nostrum eligendi impedit! Quia ratione delectus in
        rerum nisi obcaecati quod nemo explicabo earum voluptates recusandae
        placeat quis suscipit debitis mollitia, repudiandae animi ipsum
        molestias laborum odio cumque iure. Doloremque quasi fugit suscipit
        perspiciatis esse nemo fuga, qui est aliquam et necessitatibus modi
        obcaecati? Mollitia, perferendis! Temporibus, deleniti? Dolorum
        molestiae, vitae praesentium maxime perferendis officia tempore tempora
        adipisci maiores vel, nam, veniam ex ipsam voluptate. Suscipit
        voluptatibus blanditiis eveniet deserunt. Fuga id, esse ex sit quisquam
        hic dolorem tempora maxime omnis? Hic culpa expedita, voluptatibus
        perspiciatis amet perferendis natus maiores provident dolorem repellat,
        ipsum sed fuga, blanditiis voluptate fugiat numquam. Voluptates maxime
        omnis repellat aut excepturi possimus nobis praesentium assumenda
        voluptatum facere ad labore recusandae sint, eaque numquam. Ratione
        reiciendis, repellendus quo dolore sit tempora dolorum qui eum
        laboriosam deserunt rem ab ipsum at. Cumque voluptatem, facere molestias
        quasi porro veniam numquam magnam corrupti optio mollitia sunt ipsa
        voluptate. Repudiandae quasi vero architecto blanditiis suscipit
        molestias vitae eum placeat, consequuntur quam, dolores labore magnam
        dolorem reiciendis? Numquam esse fuga pariatur cumque incidunt
        laudantium veniam quia atque voluptatibus ipsam ratione officiis omnis
        iure fugiat molestias autem velit quod aliquam quibusdam nemo, tempora
        sit deleniti minima sapiente! Quo ducimus voluptatem nisi eveniet.
      </p>
    </div>
  );
};

export default AboutUs;
