import React, { useState, useEffect } from 'react';
import Cards from '../../Components/Card/Card.component';
import axios from 'axios';

// Loading
import Spinner from '../../Components/Spinner/Spinner.component';

// Css
import './pricePrestation.styles.scss';
const PricePrestation = () => {
   // Directory Images
   const imageBaseURL = 'https://irimaxserver.irimax-dev.fr/uploads/';

   const [cardsLists, setCardsLists] = useState([]);
   const [isLoading, setIsLoading] = useState(false);
 
   useEffect((err) => {
     setIsLoading(true);
     axios
       .get('https://irimaxserver.irimax-dev.fr/api/v1/cards')
 
       .then((res) => {
         console.log('*******************IRIMAX*******************');
         console.log(res);
         console.log('*******************IRIMAX*******************');
         setIsLoading(false);
         setCardsLists(res.data.data );
       })
       .catch(err);
   }, []);
 
   if (isLoading) {
     return <Spinner />;
   } else {
  return (
    <div className='container_prestation'>
      <div id='api'>
        <h1 className='title_price'>API</h1>
        <p className='text_price'>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Doloribus
          consectetur tenetur explicabo ea recusandae nesciunt sint, magnam
          dolores nobis repellat. Neque unde delectus nisi assumenda culpa,
          necessitatibus architecto iure ex. Eius, maxime dolorum aut quae fugit
          ipsum illo. Quaerat voluptatibus vel provident ratione id dolorem
          laudantium deleniti cupiditate consequuntur aliquid nulla eligendi
          quidem officia sequi suscipit, harum, nobis iusto repellendus
          recusandae excepturi odit animi, officiis autem porro. Earum officia
          autem delectus accusamus ipsa, asperiores qui, magnam magni veniam rem
          non, praesentium possimus nam corporis. Porro ut temporibus possimus
        </p>
        <div>
          <Cards cardsLists={cardsLists} imageBaseURL={imageBaseURL} techno webLink />
        </div>
      </div>
      <div id='server'>
        <h1 className='title_price'>Server</h1>
        <p className='text_price'>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Doloribus
          consectetur tenetur explicabo ea recusandae nesciunt sint, magnam
          dolores nobis repellat. Neque unde delectus nisi assumenda culpa,
          necessitatibus architecto iure ex. Eius, maxime dolorum aut quae fugit
          ipsum illo. Quaerat voluptatibus vel provident ratione id dolorem
          laudantium deleniti cupiditate consequuntur aliquid nulla eligendi
          quidem officia sequi suscipit, harum, nobis iusto repellendus
          recusandae excepturi odit animi, officiis autem porro. Earum officia
          autem delectus accusamus ipsa, asperiores qui, magnam magni veniam rem
          non, praesentium possimus nam corporis. Porro ut temporibus possimus
          ea repellat iure, vitae quod! Nesciunt minus at officia, rem porro
          asperiores tenetur ratione quidem eum dolorum molestiae blanditiis
          expedita eius itaque temporibus sapiente aliquam, officiis, animi
          debitis excepturi repellendus dolor. Nulla, quae repellendus veritatis
          perspiciatis sint cumque nostrum. Minus facilis illum iure nobis velit
          accusantium excepturi minima cupiditate numquam! Fugit ullam tempora
          eligendi ducimus ex exercitationem tempore quidem ut? Laudantium vitae
          omnis deserunt corrupti impedit tempore quos. Perspiciatis ullam
          facere amet hic velit repellendus, exercitationem, cupiditate totam
          excepturi sed quasi, veritatis fugiat corrupti deleniti officiis?
          Consequatur omnis dolore eius, laborum rerum autem dignissimos
          similique voluptatibus eveniet obcaecati qui sed expedita mollitia
          voluptate atque voluptatum assumenda architecto repellat alias dolor
          praesentium fuga animi minima. Perspiciatis at quae commodi
          praesentium. Temporibus, repudiandae consectetur! Ut eaque eius
          laborum, impedit itaque quibusdam nihil quas quo quod autem
          asperiores, rerum adipisci quasi provident quidem blanditiis
          temporibus ab sunt quis totam? Quae aut corporis nostrum, aperiam
          nulla ipsam ratione labore, dolorum quod dicta voluptatum quibusdam
          iste vitae natus sit necessitatibus numquam voluptatibus accusamus
          temporibus voluptates libero reprehenderit, recusandae ut repellendus.
          Quaerat fuga qui vitae vero reiciendis quo sapiente molestias rem
          tempora laudantium itaque, minima blanditiis magni magnam? Veritatis
          explicabo, dolores, illo corporis saepe magni iste officia
          consequuntur praesentium cum ex optio enim repellat commodi neque.
          Error cum ullam earum saepe temporibus officia similique vitae nobis
          autem ipsa minima maiores quaerat nemo nam fugiat veniam, placeat
          repudiandae illo aliquam iste obcaecati. Porro, inventore reiciendis
          voluptatem aut possimus enim, delectus saepe repellendus, velit odit
          dolorem molestias sed vitae eos asperiores accusamus error quisquam
          facere. A, officia. Excepturi veritatis voluptatem est inventore
          natus. Nesciunt, cum ipsa eligendi fuga deserunt, obcaecati quas
          impedit necessitatibus sunt ut officia, odit temporibus iure neque
          nostrum dolor nulla eos pariatur molestiae? Earum corporis quam
          distinctio maiores, aliquam cum. Magnam ullam, recusandae saepe
          aperiam sunt impedit sed vel, veritatis odit tempora explicabo earum
          eveniet aspernatur ex eius similique fugit perspiciatis aliquid
          consequuntur necessitatibus. Aspernatur corporis nisi nemo inventore
          modi esse ipsum. Ipsa at delectus perspiciatis exercitationem, minus
          dignissimos praesentium sunt possimus alias eveniet consectetur
          placeat veniam dolorum sequi odio iste voluptates dolor pariatur? Modi
          odio est eius! Sapiente laudantium, accusamus mollitia quia quis totam
          non ipsa est? Vitae deserunt voluptatum alias nam, optio odit. Dolorum
          quae enim aliquid ratione neque voluptate, magnam aspernatur facere
          maiores dolore repudiandae quos aut iure? Labore deserunt ea corrupti
          at perspiciatis repellat distinctio quibusdam aliquid eveniet, iusto,
          beatae amet, rem reprehenderit commodi minus? Dolorum deleniti libero
          quibusdam itaque dolores pariatur
        </p>
      </div>
      <div id='mesure'>
        <h1 className='title_price'>Site sur mesure</h1>
        <p className='text_price'>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Doloribus
          consectetur tenetur explicabo ea recusandae nesciunt sint, magnam
          dolores nobis repellat. Neque unde delectus nisi assumenda culpa,
          necessitatibus architecto iure ex. Eius, maxime dolorum aut quae fugit
          ipsum illo. Quaerat voluptatibus vel provident ratione id dolorem
          laudantium deleniti cupiditate consequuntur aliquid nulla eligendi
          quidem officia sequi suscipit, harum, nobis iusto repellendus
          recusandae excepturi odit animi, officiis autem porro. Earum officia
          autem delectus accusamus ipsa, asperiores qui, magnam magni veniam rem
          non, praesentium possimus nam corporis. Porro ut temporibus possimus
          ea repellat iure, vitae quod! Nesciunt minus at officia, rem porro
          asperiores tenetur ratione quidem eum dolorum molestiae blanditiis
          expedita eius itaque temporibus sapiente aliquam, officiis, animi
          debitis excepturi repellendus dolor. Nulla, quae repellendus veritatis
          perspiciatis sint cumque nostrum. Minus facilis illum iure nobis velit
          accusantium excepturi minima cupiditate numquam! Fugit ullam tempora
          eligendi ducimus ex exercitationem tempore quidem ut? Laudantium vitae
          omnis deserunt corrupti impedit tempore quos. Perspiciatis ullam
          facere amet hic velit repellendus, exercitationem, cupiditate totam
          excepturi sed quasi, veritatis fugiat corrupti deleniti officiis?
          Consequatur omnis dolore eius, laborum rerum autem dignissimos
          similique voluptatibus eveniet obcaecati qui sed expedita mollitia
          voluptate atque voluptatum assumenda architecto repellat alias dolor
          praesentium fuga animi minima. Perspiciatis at quae commodi
          praesentium. Temporibus, repudiandae consectetur! Ut eaque eius
          laborum, impedit itaque quibusdam nihil quas quo quod autem
          asperiores, rerum adipisci quasi provident quidem blanditiis
          temporibus ab sunt quis totam? Quae aut corporis nostrum, aperiam
          nulla ipsam ratione labore, dolorum quod dicta voluptatum quibusdam
          iste vitae natus sit necessitatibus numquam voluptatibus accusamus
          temporibus voluptates libero reprehenderit, recusandae ut repellendus.
          Quaerat fuga qui vitae vero reiciendis quo sapiente molestias rem
          tempora laudantium itaque, minima blanditiis magni magnam? Veritatis
          explicabo, dolores, illo corporis saepe magni iste officia
          consequuntur praesentium cum ex optio enim repellat commodi neque.
          Error cum ullam earum saepe temporibus officia similique vitae nobis
          autem ipsa minima maiores quaerat nemo nam fugiat veniam, placeat
          repudiandae illo aliquam iste obcaecati. Porro, inventore reiciendis
          voluptatem aut possimus enim, delectus saepe repellendus, velit odit
          dolorem molestias sed vitae eos asperiores accusamus error quisquam
          facere. A, officia. Excepturi veritatis voluptatem est inventore
          natus. Nesciunt, cum ipsa eligendi fuga deserunt, obcaecati quas
          impedit necessitatibus sunt ut officia, odit temporibus iure neque
          nostrum dolor nulla eos pariatur molestiae? Earum corporis quam
          distinctio maiores, aliquam cum. Magnam ullam, recusandae saepe
          aperiam sunt impedit sed vel, veritatis odit tempora explicabo earum
          eveniet aspernatur ex eius similique fugit perspiciatis aliquid
          consequuntur necessitatibus. Aspernatur corporis nisi nemo inventore
          modi esse ipsum. Ipsa at delectus perspiciatis exercitationem, minus
          dignissimos praesentium sunt possimus alias eveniet consectetur
          placeat veniam dolorum sequi odio iste voluptates dolor pariatur? Modi
          odio est eius! Sapiente laudantium, accusamus mollitia quia quis totam
          non ipsa est? Vitae deserunt voluptatum alias nam, optio odit. Dolorum
          quae enim aliquid ratione neque voluptate, magnam aspernatur facere
          maiores dolore repudiandae quos aut iure? Labore deserunt ea corrupti
          at perspiciatis repellat distinctio quibusdam aliquid eveniet, iusto,
          beatae amet, rem reprehenderit commodi minus? Dolorum deleniti libero
          quibusdam itaque dolores pariatur
        </p>
      </div>
      <div id='wordpress'>
        <h1 className='title_price'>Wordpress</h1>
        <p className='text_price'>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Doloribus
          consectetur tenetur explicabo ea recusandae nesciunt sint, magnam
          dolores nobis repellat. Neque unde delectus nisi assumenda culpa,
          necessitatibus architecto iure ex. Eius, maxime dolorum aut quae fugit
          ipsum illo. Quaerat voluptatibus vel provident ratione id dolorem
          laudantium deleniti cupiditate consequuntur aliquid nulla eligendi
          quidem officia sequi suscipit, harum, nobis iusto repellendus
          recusandae excepturi odit animi, officiis autem porro. Earum officia
          autem delectus accusamus ipsa, asperiores qui, magnam magni veniam rem
          non, praesentium possimus nam corporis. Porro ut temporibus possimus
          ea repellat iure, vitae quod! Nesciunt minus at officia, rem porro
          asperiores tenetur ratione quidem eum dolorum molestiae blanditiis
          expedita eius itaque temporibus sapiente aliquam, officiis, animi
          debitis excepturi repellendus dolor. Nulla, quae repellendus veritatis
          perspiciatis sint cumque nostrum. Minus facilis illum iure nobis velit
          accusantium excepturi minima cupiditate numquam! Fugit ullam tempora
          eligendi ducimus ex exercitationem tempore quidem ut? Laudantium vitae
          omnis deserunt corrupti impedit tempore quos. Perspiciatis ullam
          facere amet hic velit repellendus, exercitationem, cupiditate totam
          excepturi sed quasi, veritatis fugiat corrupti deleniti officiis?
          Consequatur omnis dolore eius, laborum rerum autem dignissimos
          similique voluptatibus eveniet obcaecati qui sed expedita mollitia
          voluptate atque voluptatum assumenda architecto repellat alias dolor
          praesentium fuga animi minima. Perspiciatis at quae commodi
          praesentium. Temporibus, repudiandae consectetur! Ut eaque eius
          laborum, impedit itaque quibusdam nihil quas quo quod autem
          asperiores, rerum adipisci quasi provident quidem blanditiis
          temporibus ab sunt quis totam? Quae aut corporis nostrum, aperiam
          nulla ipsam ratione labore, dolorum quod dicta voluptatum quibusdam
          iste vitae natus sit necessitatibus numquam voluptatibus accusamus
          temporibus voluptates libero reprehenderit, recusandae ut repellendus.
          Quaerat fuga qui vitae vero reiciendis quo sapiente molestias rem
          tempora laudantium itaque, minima blanditiis magni magnam? Veritatis
          explicabo, dolores, illo corporis saepe magni iste officia
          consequuntur praesentium cum ex optio enim repellat commodi neque.
          Error cum ullam earum saepe temporibus officia similique vitae nobis
          autem ipsa minima maiores quaerat nemo nam fugiat veniam, placeat
          repudiandae illo aliquam iste obcaecati. Porro, inventore reiciendis
          voluptatem aut possimus enim, delectus saepe repellendus, velit odit
          dolorem molestias sed vitae eos asperiores accusamus error quisquam
          facere. A, officia. Excepturi veritatis voluptatem est inventore
          natus. Nesciunt, cum ipsa eligendi fuga deserunt, obcaecati quas
          impedit necessitatibus sunt ut officia, odit temporibus iure neque
          nostrum dolor nulla eos pariatur molestiae? Earum corporis quam
          distinctio maiores, aliquam cum. Magnam ullam, recusandae saepe
          aperiam sunt impedit sed vel, veritatis odit tempora explicabo earum
          eveniet aspernatur ex eius similique fugit perspiciatis aliquid
          consequuntur necessitatibus. Aspernatur corporis nisi nemo inventore
          modi esse ipsum. Ipsa at delectus perspiciatis exercitationem, minus
          dignissimos praesentium sunt possimus alias eveniet consectetur
          placeat veniam dolorum sequi odio iste voluptates dolor pariatur? Modi
          odio est eius! Sapiente laudantium, accusamus mollitia quia quis totam
          non ipsa est? Vitae deserunt voluptatum alias nam, optio odit. Dolorum
          quae enim aliquid ratione neque voluptate, magnam aspernatur facere
          maiores dolore repudiandae quos aut iure? Labore deserunt ea corrupti
          at perspiciatis repellat distinctio quibusdam aliquid eveniet, iusto,
          beatae amet, rem reprehenderit commodi minus? Dolorum deleniti libero
          quibusdam itaque dolores pariatur
        </p>
      </div>
    </div>
  );
}};

export default PricePrestation;
