import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import Map from '../../Components/GoogleMap/GoogleMap.component';
import Btn from '../../Components/Button/ButtonInput.component';

import { ReactComponent as LogoSkype } from '../../assets/img/Skype.svg';

// Aos Animation
import Aos from 'aos';
import 'aos/dist/aos.css';

import './contact.styles.scss';

//  Google font typeFace
require('typeface-satisfy');

export default function Contact() {
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = (data, e) => {
    axios({
      method: 'POST',
      url: 'https://irimaxserver.irimax-dev.fr/api/v1/dashboard/send',
      data,
    })
      .then(() => {
        e.target.reset();
        alert('Email envoyé merci, nous vous répondrons rapidement');
      })
      .catch((error) => {
        console.log(error);
      });
  };
  console.log(errors);
  useEffect(() => {
    Aos.init({ once: true });
   
  }, []);
  return (
    <div className='container_contact'>
      <img src="./assets/img/380.png" alt="" className="image_contact"/>
      <h1 className='title_contact' data-aos="zoom-in"  data-aos-duration="2000">Un projet ? Une idée ?</h1>
          <h2 className='info_contact' data-aos="zoom-in"  data-aos-duration="2000">
            Une information, un devis... Laissez votre message
          </h2>
      <div className='containerForm' data-aos="zoom-in"  data-aos-duration="3000">
        <form className='form' onSubmit={handleSubmit(onSubmit)}>
          

          <div className='inputRadio'>
            <label htmlFor='Mr'>Mr</label>
            <input name='gender' type='radio' value='Mr' ref={register} />

            <label htmlFor='Mme'>Mme</label>
            <input name='gender' type='radio' value='Mme' ref={register} />
          </div>
          <input
            className='inputBox'
            type='text'
            placeholder='First name'
            name='firstName'
            autoComplete='off'
            ref={register({ required: true, maxLength: 80 })}
          />

          {errors.firstName && (
            <p className='error'>
              <FontAwesomeIcon
                icon={['fal', 'exclamation-circle']}
                className='errorIcon'
              />
              First name is required
            </p>
          )}
          <input
            className='inputBox'
            type='text'
            placeholder='Last name'
            name='lastName'
            autoComplete='off'
            ref={register({ required: true, maxLength: 100 })}
          />
          {errors.lastName && (
            <p className='error'>
              <FontAwesomeIcon
                icon={['fal', 'exclamation-circle']}
                className='errorIcon'
              />
              Last name is required
            </p>
          )}
          <input
            className='inputBox'
            type='text'
            placeholder='Email'
            name='email'
            autoComplete='off'
            ref={register({ required: true, pattern: /^\S+@\S+$/i })}
          />
          {errors.email && (
            <p className='error'>
              <FontAwesomeIcon
                icon={['fal', 'exclamation-circle']}
                className='errorIcon'
              />
              Email is required
            </p>
          )}
          <input
            className='inputBox'
            type='text'
            placeholder='Phone'
            name='phone'
            autoComplete='off'
            ref={register({
              minLength: 6,
              maxLength: 12,
            })}
          />
          <>
            <label className='labelTextarea'>Votre message</label>
            <textarea
              className='inputTextarea'
              name='message'
              ref={register({ required: true, maxLength: 500 })}
            />
          </>
          {errors.message && (
            <p className='error'>
              <FontAwesomeIcon
                icon={['fal', 'exclamation-circle']}
                className='errorIcon'
              />
              Message is required
            </p>
          )}
          <Btn typeBtn='submit' text_btn='Envoyer' />
          {/* <input type='submit' /> */}
        </form>
      </div>
      <div className='container_encart' data-aos="zoom-in"  data-aos-duration="2000">
        <div className='encart'>
          <div className='adress'>
          <FontAwesomeIcon className='mapIcon' size="3x" icon={['fad', 'map-marked-alt']} />
            <h1 className='titleSociety'>Irimax Development</h1>
            <p className='adressSociety'>15 rue du 14 juillet</p>
            <p className='postalCode'>59264</p>
            <p className='city'>Onnaing</p>
            <p className='phoneSociety'>
             
              <FontAwesomeIcon className='mobileIcon' size="lg" icon={['fad', 'mobile']} />
              06.19.75.30.92 ~ contact@irimax-dev.fr
            </p>
            <a className='skype' href='skype:irimax development?chat'>
              <LogoSkype className='skypeLogo' />
              irimaxidevelopment
            </a>
          </div>
          <div className='googleMap'>
            <Map />
          </div>
        </div>
      </div>
    </div>
  );
}
