import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fal } from '@fortawesome/pro-light-svg-icons';
import { fad } from '@fortawesome/pro-duotone-svg-icons';
import { far } from '@fortawesome/pro-regular-svg-icons';
import ScrollToTop from 'react-scroll-up';
// Css
import './App.scss';

// Css React-Slick
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

// Google Fonts
import 'typeface-cinzel';

// components
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import NotFound from './Components/NotFound/Notfound';

// Container
import Home from './Container/HomePage/HomePage';
import Contact from './Container/Contact/Contact';
import AboutUs from './Container/AboutUs/AboutUs';
import Portfolio from './Container/Portfolio/Portfolio';
import PricePrestation from './Container/PricePrestation/PricePrestation';

function App() {
  library.add(fab, fal, fad,far);

  return (
    <div>
      <ScrollToTop showUnder={60} style={{ zIndex: 1000 }}>
        <FontAwesomeIcon
          icon={['fad', 'angle-double-up']}
          className='upIcon'
          size='2x'
        />
      </ScrollToTop>

      <div className='container'>
        <Header />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/contact' component={Contact} />
          <Route exact path='/aboutUs' component={AboutUs} />
          <Route exact path='/portfolio' component={Portfolio} />
          <Route exact path='/PricePrestation' component={PricePrestation} />
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </div>
    </div>
  );
}

export default App;
